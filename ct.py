# ***********************************************
# ***  Loco363 - CAD Drawings - Generic code  ***
# ***            Coordinates Texts            ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from gen_draw import Drawing
from gen_draw.coords import Coords
from gen_draw.shapes import Circle, Text


class CoordsText(Text):
	'''Text object containing absolute coordinates'''
	
	def __init__(self, c, pos = None, *args, **kwargs):
		'''Constructor
		
		@param c - Coords instance to use for content
		@param pos - position of text itself - could be a Coords instance or (x, y) tuple to position the text relative to Coords c
		'''
		
		if not isinstance(c, Coords):
			raise ValueError('Coords instance needed!')
		self._c = c
		
		if pos:
			if not isinstance(pos, Coords):
				pos = Coords(c, pos)
		else:
			pos = c
		
		super().__init__(pos, '', *args, **kwargs)
	# constructor
	
	def getText(self):
		c = self._c.getAbsolute()
		return '[' + self._fmt(c[0]) + ' ; ' + self._fmt(c[1]) + ']'
	# getText
	
	def _fmt(self, num):
		'''Formats single coordinate
		
		@param num - coordinate to format
		@return formated coordinate as string
		'''
		
		# round to 3 decimal places and convert to string
		t = str(round(num, 3))
		
		# remove trailing zeros
		t = t.rstrip('0').rstrip('.')
		if t == '':#TODO needed?
			t = '0'
		
		# fix very small negative numbers leading to "negative zero"
		if t == '-0':
			t = '0'
		
		return t
	# _fmt
# CoordsText


class CoordsAuto(Drawing):
	'''Blue point with text object containing its coordinates'''
	
	def __init__(self, c, r = 1, pos = (2, -5), color = '#0BF', size = 12):
		'''Constructor
		
		@param c - Coords instance to use for content
		@param r - radius of marking point
		@param pos - relative position of text to point (x, y, (optional) text-anchor)
		@param color - text and point color
		@param size - font size in pt
		'''
		
		super().__init__(c)
		
		size = float(size)
		if size <= 0:
			raise ValueError('Font size should be a positive number!')
		
		# point
		self.add(Circle(
			c,
			r,
			properties={
				'fill': color
			}
		))
		
		# text
		anchor = 'start'
		if len(pos) > 2:
			# text-anchor specified
			anchor = pos[2]
			pos = pos[:2]
		
		props = {
			'fill': color,
			'style': 'font-size:'+str(size)+'pt;'
		}
		
		if anchor != 'start':
			props['text-anchor'] = anchor
		
		self.add(CoordsText(
			c,
			Coords(c, pos),
			properties=props
		))
	# constructor
# CoordsAuto
