# ***********************************************
# ***  Loco363 - CAD Drawings - Generic code  ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from gen_draw import Drawing
from gen_draw.coords import Coords as C
from gen_draw.shapes import Polygon

from .ct import CoordsAuto


class LabeledPolygon(Drawing):
	'''Polygon with CoordsAuto at each point'''
	
	def __init__(self, c, points, *args, **kwargs):
		'''Constructor
		
		@param c - main coordinates used for relative positioning
		@param points - list of coordinates specifying polygon points
			Each point could be:
				- coordinates instance
					Coords
				- tuple of X and Y difference from main coordinates
					(dX, dY)
				- tuple of coordinates instance and point's text position
					(Coords, pos)
				- tuple of X and Y difference from main coordinates and point's text position
					(dX, dY, pos)
			Text position will be passed directly to CoordsAuto.
		'''
		
		super().__init__(c)
		
		# extract text position from points
		ppoints = []
		cpos = []
		for p in points:
			point = p
			pos = None
			
			try:
				if not isinstance(p, C):
					if len(p) > 2:
						point = p[:2]
						pos = p[2]
					elif len(p) > 1 and isinstance(p[0], C):
						point = p[0]
						pos = p[1]
			except:
				# just catch problems with calling len() on unsupported object
				pass
			
			ppoints.append(point)
			cpos.append(pos)
		
		# polygon
		p = Polygon(c, ppoints, *args, **kwargs)
		self.add(p)
		
		# coords
		points = p.getPoints()
		for i in range(len(points)):
			if cpos[i]:
				self.add(CoordsAuto(points[i], pos=cpos[i]))
			else:
				self.add(CoordsAuto(points[i]))
	# constructor
# LabeledPolygon
