# ***********************************************
# ***  Loco363 - CAD Drawings - Generic code  ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


from gen_draw import Drawing
from gen_draw.coords import Coords
from gen_draw.shapes import Circle, Line, Text

from .ct import CoordsAuto


class Hole(Drawing):
	'''Hole - circle with cross to specify center'''
	
	def __init__(self, c, d):
		'''Constructor
		
		@param c - coordinates of hole center
		@param d - hole diameter
		'''
		
		super().__init__(c)
		
		# handle extra circles
		try:
			for i in range(len(d) - 1):
				self.add(Circle(
					c,
					float(d[i+1])/2,
					properties={
						'fill': 'none',
						'stroke': 'black',
						'stroke-width': 1
					}
				))
			
			d = d[0]
		except(TypeError):
			pass
		
		# circle
		self.add(Circle(
			c,
			float(d)/2,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-width': 1.5
			}
		))
		
		# cross
		self.add(Line(
			Coords(c, (-1-d/2, 0)),
			Coords(c, (+1+d/2, 0)),
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-dasharray': '1,1',
				'stroke-width': 1
			}
		))
		self.add(Line(
			Coords(c, (0, -1-d/2)),
			Coords(c, (0, +1+d/2)),
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-dasharray': '1,1',
				'stroke-width': 1
			}
		))
	# constructor
# Hole


class DrillHole(Hole):
	'''Hole with its coordinates and type'''
	
	def __init__(self, c, d, type = '', pos = (2, 2), cpos = (2, -5), color = '#C00', size = 12):
		'''Constructor
		
		@param c - coordinates of hole center
		@param d - hole diameter
		@param type - text to specify hole type
		@param pos - relative position of type text to hole center (x, y, (optional) text-anchor)
		@param cpos - relative position of center coordinates text to hole center (x, y, (optional) text-anchor)
		@param color - type text color
		@param size - font size in pt
		'''
		
		super().__init__(c, d)
		
		# coordinates
		self.add(CoordsAuto(c, pos=cpos))
		
		# handle extra circles
		try:
			d = d[0]
		except(TypeError):
			pass
		
		# type
		anchor = 'start'
		if len(pos) > 2:
			# text-anchor specified
			anchor = pos[2]
			pos = pos[:2]
		
		props = {
			'fill': color,
			'style': 'font-size:'+str(size)+'pt;'
		}
		
		if anchor != 'start':
			props['text-anchor'] = anchor
		
		self.add(Text(
			Coords(c, pos),
			str(d) + str(type),
			properties=props
		))
	# constructor
# DrillHole
