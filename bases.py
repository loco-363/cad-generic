# ***********************************************
# ***  Loco363 - CAD Drawings - Generic code  ***
# ***          Bases for other CADs           ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw


class Base(gen_draw.Drawing):
	'''The ultimate ancestor of all other CADs'''
	
	# universal color for placement shapes
	COLOR_UNI = '#CCC'
	
	
	def __init__(self, c = None):
		'''Constructor
		
		@param c - center Coords
		'''
		
		super().__init__(c)
	# constructor
# class Base



class Drill(Base):
	'''The ultimate ancestor of all Drill CADs'''
# class Drill



class Label(Base):
	'''The ultimate ancestor of all Label CADs'''
	
	# colors for each level of Label CADs
	COLOR_PANEL_1 = '#C00'
	COLOR_PANEL_2 = '#5AF'
	COLOR_PANEL_3 = '#080'
	COLOR_PART = '#731'
# class Label


class LabelPart(Label):
	'''The ultimate ancestor of all Label CADs for individual parts'''
	
	def __init__(self, c = None, name = '??'):
		'''Constructor
		
		@param c - center Coords
		@param name - placed part label
		'''
		
		super().__init__(c)
		c = self.getCoords()
		
		# color
		self.color = self.COLOR_PART
		
		# label
		self.addAfter(gen_draw.shapes.Text(
			gen_draw.coords.Coords(c, (0,-6.5)),
			name,
			properties={
				'fill': self.color,
				'style': 'font-size:50pt; font-weight:bold;',
				'text-anchor': 'middle'
			}
		))
	# constructor
# class LabelPart


class LabelPanel(Label):
	'''The ultimate ancestor of all Label CADs for panels'''
	
	def __init__(self, c, name, points, color, bw, nfs, npos):
		'''Constructor
		
		@param c - bottom-left corner Coords
		@param name - placed panel label
		@param points - points of border polygon
		@param color
		@param bw - width of border line
		@param nfs - label font-size
		@param npos - position of label
			- tuple of X and Y relative to coords + optional text-anchor
			- tuple of X and None => +/- X relative to outline min-/max-X, Y to the center of outline min-/max-Y
		'''
		
		super().__init__(c)
		c = self.getCoords()
		
		# border
		self.add(gen_draw.shapes.Polygon(
			c,
			points,
			properties={
				'fill': 'none',
				'stroke': color,
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-width': bw
			}
		))
		
		# label position
		if npos[1] == None:
			# make it mutable
			npos = list(npos)
			
			# init extremes
			xmin = points[0][0]
			xmax = xmin
			ymin = points[0][1]
			ymax = ymin
			
			# find extremes in border points
			for p in points:
				if p[0] < xmin:
					xmin = p[0]
				if p[0] > xmax:
					xmax = p[0]
				if p[1] < ymin:
					ymin = p[1]
				if p[1] > ymax:
					ymax = p[1]
			
			# compute position
			npos[0] = (xmin if npos[0] < 0 else xmax) + npos[0]
			npos[1] = ymin + (ymax-ymin)/2
			
			# lower it by 20mm (we know font-size in pt only; 20mm seems OK for both levels (1 and 2))
			npos[1] -= 20
		
		# label text-anchor
		anchor = 'start'
		if len(npos) > 2:
			# text-anchor specified
			anchor = npos[2]
			npos = npos[:2]
		
		# label
		props = {
			'fill': color,
			'style': 'font-size:'+str(nfs)+'pt; font-weight:bold;'
		}
		
		if anchor != 'start':
			props['text-anchor'] = anchor
		
		self.add(gen_draw.shapes.Text(
			gen_draw.coords.Coords(c, npos),
			name,
			properties=props
		))
	# constructor
# class LabelPanel


class LabelPanel1(LabelPanel):
	'''The ancestor of level-1 panels'''
	
	def __init__(self, *args, npos = (10, None), **kwargs):
		'''Constructor
		
		@param npos - position of label
		'''
		
		super().__init__(*args, color=self.COLOR_PANEL_1, bw=20, nfs=200, npos=npos, **kwargs)
	# constructor
# class LabelPanel1


class LabelPanel2(LabelPanel):
	'''The ancestor of level-2 panels'''
	
	def __init__(self, *args, npos = (15, None), **kwargs):
		'''Constructor
		
		@param npos - position of label
		'''
		
		super().__init__(*args, color=self.COLOR_PANEL_2, bw=10, nfs=150, npos=npos, **kwargs)
	# constructor
# class LabelPanel2


class LabelPanel3(LabelPanel):
	'''The ancestor of level-3 panels'''
	
	def __init__(self, *args, npos = (10, 10), **kwargs):
		'''Constructor
		
		@param npos - position of label
		'''
		
		super().__init__(*args, color=self.COLOR_PANEL_3, bw=6, nfs=100, npos=npos, **kwargs)
	# constructor
# class LabelPanel3



class View(Base):
	'''The ultimate ancestor of all View CADs'''
# class View


class ViewPanel(View):
	'''The ultimate ancestor of all View CADs for panels'''
	
	def __init__(self, c, points, bw):
		'''Constructor
		
		@param c - bottom-left corner Coords
		@param points - points of border polygon
		@param bw - width of border line
		'''
		
		super().__init__(c)
		c = self.getCoords()
		
		# border
		self.add(gen_draw.shapes.Polygon(
			c,
			points,
			properties={
				'fill': 'none',
				'stroke': 'black',
				'stroke-linecap': 'round',
				'stroke-linejoin': 'round',
				'stroke-width': bw
			}
		))
	# constructor
# class ViewPanel


class ViewPanel1(ViewPanel):
	'''The ancestor of level-1 panels'''
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, bw=20, **kwargs)
	# constructor
# class ViewPanel1


class ViewPanel2(ViewPanel):
	'''The ancestor of level-2 panels'''
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, bw=10, **kwargs)
	# constructor
# class ViewPanel2

class ViewPanel3(ViewPanel):
	'''The ancestor of level-3 panels'''
	
	def __init__(self, *args, **kwargs):
		super().__init__(*args, bw=6, **kwargs)
	# constructor
# class ViewPanel3
